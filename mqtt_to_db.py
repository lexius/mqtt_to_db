#!python3

import json
import os
import sys
from typing import Sequence, Tuple

import jmespath
import psycopg
import yarl
from paho.mqtt import subscribe
from psycopg.errors import UniqueViolation
import dotenv

DEFAULT_MQTT_PORT = 1883

dotenv.load_dotenv()

MQTT_URL = yarl.URL(os.environ['MQTT_URL'])
MQTT_TOPIC = os.environ['MQTT_TOPIC']
DATABASE_URL = os.environ['DATABASE_URL']
DATABASE_TABLE = os.getenv('DATABASE_TABLE', 'data')
JSON_FIELDS = os.environ['JSON_FIELDS']


def parse_payload(expr, payload) -> Tuple[Sequence, Sequence]:
    data = json.loads(payload)
    res = jmespath.search(expr, data)
    return tuple(res.keys()), tuple(res.values())


def save_data(fields: Sequence[str], values: Sequence) -> None:
    sql_expr = 'INSERT INTO {}({}) VALUES ({})'.format(
        DATABASE_TABLE,
        ', '.join(fields),
        ', '.join(('%s', ) * len(fields)),
    )
    with psycopg.connect(DATABASE_URL) as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(sql_expr, values)
            except UniqueViolation:
                print('Already there')
            else:
                print(f'{values} -> {fields} saved.')


def main():
    mqtt_host = MQTT_URL.host
    mqtt_port = MQTT_URL.port or DEFAULT_MQTT_PORT
    res = subscribe.simple(MQTT_TOPIC, hostname=mqtt_host, port=mqtt_port)
    fields, values = parse_payload(JSON_FIELDS, res.payload)
    save_data(fields, values)


if __name__ == '__main__':
    sys.exit(main() or 0)
