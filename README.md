# mqtt_to_db

## Description
This is a very simple utility that fetches a JSON object from a specified MQTT topic and saves some of its data to a specified table in a Postgres database. By no means it should be considered a full-scale ETL tool, it's just a simple helper I use in my personal projects, so, there're no plans and desire to make it more complex or feature-rich, only small-scale fixes or improvements may eventually happen.

## How it works
The idea is that mqtt_to_db should be called periodically by something like Cron or whatever, it'll fetch data from a specified MQTT topic, parse it assuming it's a JSON object and dump specified parts of it to a database table. To parse the JSON object and define what parts of it should be saved to what fields, [JMESPath](https://jmespath.org/) query language is used. A query is specified using JSON_FIELDS environment variable and its result should be objects whose keys will be names of table fields and whose values will be values to be saved.
Also, keep in mind that if uniqueness constraints defined in a database are violated, this will be ignored and no error raised because of that.

## Configuration
All the configuration is done using environment variables. Also, `.env` file is supported and automatically loaded if available(see `.env.example`), though if an environment variable is defined, it'll take precedence over values in `.env`.
Available configuration variables are:
* `MQTT_URL` - MQTT broker connection string (ex. 'mqtt://my.mqtt.host:1883')
* `MQTT_TOPIC` MQTT topic name (ex. 'some/mqtt/topic/name')
* `DATABASE_URL` Database connection string. Only Postgres is supported. (ex. 'postgresql://my_user:my_password@my.db.host/my_fancy_database')
* `DATABASE_TABLE` Database table name to be used. Defaults to 'data' if not specified. 
* `JSON_FIELDS` JMESPath query defining data to be dumped. (ex. '{"db_field1": json_field1, "db_field2": json_field2}' will make parser extract keys 'json_field1' and 'json_field2' and save them to db_field1 and db_field2 respectively)

## Installation
There're no specific means of installation currently. Just install dependencies from the `requirements.txt` file in whatever environment you prefer, define configuration using environment variables or ´.env´ file, and off you go.

## Contributing
If you have any suggestions or ideas, feel free to create an issue and/or a PR. 
